﻿using System;
using System.Linq;

namespace GertmanElection
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfTestCases = GetNumberOfTestCases();
            string[] testCaseWinners = new string[numberOfTestCases];

            for (int k = 0; k < numberOfTestCases; k++)
            {
                int numberOfCandidates = GetNumberOfCandidates(k + 1);

                int[] votes = new int[numberOfCandidates];
                for (int i = 0; i < numberOfCandidates;)
                {
                    votes[i] = GetNumberOfVotes(++i);
                }

                testCaseWinners[k] = GetWinner(votes);
            }

            foreach(var winner in testCaseWinners)
            {
                Console.WriteLine(winner);
            }
        }

        private static int GetNumberOfVotes(int candidate)
        {
            Console.WriteLine($"Enter number of votes for candidate {candidate}");
            int numberOfVotes = 0;

            bool validAmountOfVotes = false;
            while (!validAmountOfVotes)
            {
                try
                {
                    numberOfVotes = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid number.");
                    continue;
                }

                if (numberOfVotes < 0 || numberOfVotes > 50000)
                {
                    Console.WriteLine("There cannot be negative votes or more than 50.000.");
                    continue;
                }

                validAmountOfVotes = true;
            }

            return numberOfVotes;
        }

        private static int GetNumberOfTestCases()
        {
            Console.WriteLine("Enter number of test cases:");
            int numberOfTestCases = 0;

            bool validAmountOfTestCases = false;
            while (!validAmountOfTestCases)
            {
                try
                {
                    numberOfTestCases = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid number.");
                    continue;
                }

                if (numberOfTestCases <= 0 || numberOfTestCases > 500)
                {
                    Console.WriteLine("There must be at least 1 test case and no more than 500.");
                    continue;
                }

                validAmountOfTestCases = true;
            }

            return numberOfTestCases;
        }

        private static int GetNumberOfCandidates(int testCase)
        {
            Console.WriteLine($"\nEnter number of candidates for test case {testCase}:");
            int numberOfCandidates = 0;
            
            bool validAmountOfCandidates = false;
            while (!validAmountOfCandidates)
            {
                try
                {
                    numberOfCandidates = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid number.");
                    continue;
                }

                if (numberOfCandidates < 2 || numberOfCandidates > 10)
                {
                    Console.WriteLine("There must be at least 2 candidates and no more than 10.");
                    continue;
                }

                validAmountOfCandidates = true;
            }

            return numberOfCandidates;
        }

        private static string GetWinner(int[] votes)
        {
            var totalVotes = votes.Sum();
            var votesMax = votes.Max();

            if (votesMax > (totalVotes / 2))
            {
                return $"Majority winner {votes.ToList().IndexOf(votesMax) + 1}";
            }

            var winner = 0;
            var counter = 1;
            for(int i = 0; i < votes.Length; i++)
            {
                winner = (i > 0 && votes[i] > votes[i - 1]) ? i : winner;
                counter = (i > 0 && votes[i] == votes[i - 1]) ? counter + 1 : counter;
            }

            if (counter == votes.Length)
            {
                return "No winner";
            }

            return $"Minority winner {winner + 1}";
        }
    }
}
